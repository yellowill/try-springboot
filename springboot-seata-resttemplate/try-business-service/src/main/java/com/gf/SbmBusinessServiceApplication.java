package com.gf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import io.seata.spring.annotation.GlobalTransactionScanner;

@SpringBootApplication
public class SbmBusinessServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbmBusinessServiceApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }
    @Value("${spring.application.name}")
    private String appName;
    @Bean
    public GlobalTransactionScanner globalTransactionScanner() {
        return new GlobalTransactionScanner(appName,"default-tx-group");
    }
    
}
