package com.gf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import io.seata.spring.annotation.GlobalTransactionScanner;

@SpringBootApplication
//(exclude = DataSourceAutoConfiguration.class)
public class SbmAccountServiceApplication {
	
	

    public static void main(String[] args) {
        SpringApplication.run(SbmAccountServiceApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }
    
    
//    @Value("${spring.application.name}")
//    private String appName;
//    @Bean
//    @ConditionalOnProperty(name = "seata.enabled", havingValue = "true")
//    public GlobalTransactionScanner globalTransactionScanner() {
//        return new GlobalTransactionScanner(appName,"default-tx-group");
//    }
//    试一试
//    我是idea 我也试一试
   // @Bean
   // @ConfigurationProperties(prefix = "spring.datasource")
    //public DataSource druidDataSource() {
    //    DruidDataSource druidDataSource = new DruidDataSource();
      //  return druidDataSource;
    //}

}
