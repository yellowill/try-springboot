package com.gf.controller;

import com.gf.service.AccountService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequiredArgsConstructor(onConstructor_={@Autowired})
public class AccountController {

    private final AccountService accountService;

    @GetMapping
    @GlobalTransactional(rollbackFor = Exception.class)
//    @Transactional(rollbackFor = Exception.class)
    public void debit(@RequestParam String userId , @RequestParam BigDecimal orderMoney) {
        System.out.println("account XID " + RootContext.getXID());
        accountService.debit(userId , orderMoney);
    }

    @GetMapping(value = "/c2b")
    @GlobalTransactional(rollbackFor = Exception.class)
    @Transactional(rollbackFor = Exception.class)
    public void c2b(@RequestParam String userId , @RequestParam BigDecimal orderMoney) {
        System.out.println("account XID " + RootContext.getXID());
        accountService.c2b(userId , orderMoney);
        System.out.println("11111");
    }

    /**
     * http://127.0.0.1:8081/subtract1?userId=1002&&orderMoney=1
     * @param userId
     * @param orderMoney
     */
    @GetMapping(value = "/subtract1")
    @GlobalTransactional(rollbackFor = Exception.class)
    public void accSub(@RequestParam String userId , @RequestParam BigDecimal orderMoney){
        System.out.println("account XID " + RootContext.getXID());
        accountService.subById(userId , orderMoney);
    }

    /**
     * http://127.0.0.1:8081/subtract2?userId=1002&&orderMoney=1
     * @param userId
     * @param orderMoney
     */
    @GetMapping(value = "/subtract2")
    public void accSub2(@RequestParam String userId , @RequestParam BigDecimal orderMoney){
        accountService.subById2(userId , orderMoney);
    }

}
