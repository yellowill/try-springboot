package com.gf.controller;

import com.gf.client.AccountClient;
import com.gf.entity.Storage;
import com.gf.service.StorageService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.sql.SQLException;

@RequestMapping("/api/storage")
@RestController
@RequiredArgsConstructor(onConstructor_={@Autowired})
public class StorageController {

    private final StorageService storageService;

    private final AccountClient accountClient;

    @GetMapping(value = "/deduct")
    public void deduct(@RequestParam String commodityCode, @RequestParam Integer count) throws SQLException {
        System.out.println("storage XID " + RootContext.getXID());
        storageService.deduct(commodityCode, count);
    }

    @GetMapping(value = "/b2c")
//    @GlobalTransactional(rollbackFor = Exception.class)
    @Transactional(rollbackFor = Exception.class)
    public void b2c(@RequestParam String userId , @RequestParam BigDecimal orderMoney) throws SQLException {
        System.out.println("storage XID " + RootContext.getXID());
        accountClient.c2b(userId, orderMoney);
//        throw new RuntimeException("1111");
    }

}