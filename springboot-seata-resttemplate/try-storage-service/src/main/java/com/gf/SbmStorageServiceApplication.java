package com.gf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;

import io.seata.spring.annotation.GlobalTransactionScanner;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SbmStorageServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbmStorageServiceApplication.class, args);
    }
    
    @Value("${spring.application.name}")
    private String appName;
    @Bean
    public GlobalTransactionScanner globalTransactionScanner() {
        return new GlobalTransactionScanner(appName,"default-tx-group");
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }
	/*
	 * @Bean
	 * 
	 * @ConfigurationProperties(prefix = "spring.datasource") public DataSource
	 * druidDataSource() { DruidDataSource druidDataSource = new DruidDataSource();
	 * return druidDataSource; }
	 */
}
