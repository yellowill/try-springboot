package com.ww.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;
import org.springframework.ldap.support.LdapNameBuilder;

import javax.naming.Name;

@Entry(objectClasses = {"inetOrgPerson", "top", "posixAccount"})
@Data
public class LdapUser {
    @Id
    @JsonIgnore // 必写
    private Name distinguishedName;

    @Attribute(name = "uid")
    private String uid;


}
