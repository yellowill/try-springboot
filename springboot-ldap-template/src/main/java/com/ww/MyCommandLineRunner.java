package com.ww;

import com.ww.entity.LdapUser;
import com.ww.util.LdapTemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.stereotype.Component;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;

/**
 *
 */
//@Component
@Slf4j
public class MyCommandLineRunner implements CommandLineRunner {
    @Autowired
    private LdapTemplate ldapTemplate;

    @Value("${ldap.df.uid:www}")
    private String uid;
    @Value("${ldap.df.pwd:123}")
    private String pwd;

    @Value("${spring.ldap.urls}")
    private String urls;
    @Value("${spring.ldap.base}")
    private String base;
    @Value("${spring.ldap.username}")
    private String username;
    @Value("${spring.ldap.password}")
    private String password;


    @Override
    public void run(String... var1) throws Exception {
        log.info("===============================启动执行=====================================");

        try {
            testSearch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            testSearch1();
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("----------------------------------------------------------------------------");
        try {
            testFindOne();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            testFindOne1();
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("----------------------------------------------------------------------------");
        try {
            testAuth();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            testAuth1();
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("===============================执行结束=====================================");

    }


    public void testAuth() throws Exception {
        log.info("uid:{},pwd:{}", uid, pwd);
        EqualsFilter filter = new EqualsFilter("uid", uid);
        boolean authenticate = ldapTemplate.authenticate("", filter.toString(), pwd);
        log.info("认证结果 : {}", authenticate);
    }

    public void testAuth1() throws Exception {
        log.info("uid:{},pwd:{},username:{},password:{},base:{},urls:{}", uid, pwd, username, password, base, urls);
        LdapTemplate lt = LdapTemplateUtil.getLdapTemplate(username, password, base, urls);
        EqualsFilter filter = new EqualsFilter("uid", uid);
        boolean authenticate = lt.authenticate("", filter.toString(), pwd);
        log.info("认证结果1 : {}", authenticate);
    }

    public void testSearch() throws Exception {
        log.info("uid:{}", uid);
        LdapQuery query = LdapQueryBuilder.query().where("uid").is(uid);
        ldapTemplate.search(query, (AttributesMapper<String>) attr -> {
           /*log.info("cn:{}",String.valueOf(attr.get("cn").get()));
           log.info("uid:{}",String.valueOf(attr.get("uid").get()));
           log.info("uidNumber:{}",String.valueOf(attr.get("uidNumber").get()));*/
            NamingEnumeration<? extends Attribute> all = attr.getAll();
            log.info("ldap可获得的用户信息:");
            while (all.hasMore()) {
                Attribute next = all.next();
                if ("uid".equals(next.getID())) {
                    log.info("{}={}", next.getID(), next.get());
                }
            }
            return null;
        });
    }

    public void testSearch1() throws Exception {
        log.info("uid:{},username:{},password:{},base:{},urls:{}", uid, username, password, base, urls);
        LdapTemplate lt = LdapTemplateUtil.getLdapTemplate(username, password, base, urls);
        LdapQuery query = LdapQueryBuilder.query().where("uid").is(uid);
        lt.search(query, (AttributesMapper<String>) attr -> {
           /*log.info("cn:{}",String.valueOf(attr.get("cn").get()));
           log.info("uid:{}",String.valueOf(attr.get("uid").get()));
           log.info("uidNumber:{}",String.valueOf(attr.get("uidNumber").get()));*/
            NamingEnumeration<? extends Attribute> all = attr.getAll();
            log.info("ldap可获得的用户信息1:");
            while (all.hasMore()) {
                Attribute next = all.next();
                if ("uid".equals(next.getID())) {
                    log.info("{}={}", next.getID(), next.get());
                }
            }
            return null;
        });
    }

    public void testFindOne() throws Exception {
        log.info("uid:{}", uid);
        LdapQuery query = LdapQueryBuilder.query().where("uid").is(uid);
        LdapUser one = ldapTemplate.findOne(query, LdapUser.class);
        log.info("testFindOne:{}", one);
    }

    public void testFindOne1() throws Exception {
        log.info("uid:{},username:{},password:{},base:{},urls:{}", uid, username, password, base, urls);
        LdapTemplate lt = LdapTemplateUtil.getLdapTemplate(username, password, base, urls);
        LdapQuery query = LdapQueryBuilder.query().where("uid").is(uid);
        LdapUser one = lt.findOne(query, LdapUser.class);
        log.info("testFindOne1:{}", one);
    }

}
