package com.ww.controller;

import com.ww.entity.LdapUser;
import com.ww.util.LdapTemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Slf4j
public class AdTest {

    @Autowired
    private LdapTemplate ldapTemplate;


    @Test
    public void testAuthenticate() throws Exception {
        log.info("开始认证");
        EqualsFilter filter = new EqualsFilter("sAMAccountName", "Administrator");
        boolean authenticate = ldapTemplate.authenticate("", filter.toString(), "Eoi123456!");
        log.info("认证结果 : {}", authenticate);
    }


    @Test
    public void testSearch() throws Exception {
        LdapQuery query = LdapQueryBuilder.query().where("sAMAccountName").is("Administrator");
        ldapTemplate.search(query, (AttributesMapper<String>) attr -> {
           /*log.info("cn:{}",String.valueOf(attr.get("cn").get()));
           log.info("uid:{}",String.valueOf(attr.get("uid").get()));
           log.info("uidNumber:{}",String.valueOf(attr.get("uidNumber").get()));*/
            NamingEnumeration<? extends Attribute> all = attr.getAll();
            log.info("ldap可获得的用户信息:");
            while (all.hasMore()) {
                Attribute next = all.next();
                log.info("{}={}", next.getID(), next.get());
            }
            return null;
        });
    }


}
