package com.ww.controller;

import com.ww.entity.LdapUser;
import com.ww.util.EncryptUtils;
import com.ww.util.LdapTemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import java.util.HashMap;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Slf4j
public class LdapControllerTest {

    @Autowired
    private LdapTemplate ldapTemplate;


    @Test
    public void testAuthenticate() throws Exception {
        log.info("开始认证");
        EqualsFilter filter = new EqualsFilter("uid", "admin");
        boolean authenticate = ldapTemplate.authenticate("", filter.toString(), "123456");
        log.info("认证结果 : {}", authenticate);
    }

    @Test
    public void testAuthenticate1() throws Exception {
        log.info("开始认证");
        EqualsFilter filter = new EqualsFilter("uid", "ww");
        boolean authenticate = ldapTemplate.authenticate("", filter.toString(), "123");
        log.info("认证结果 : {}", authenticate);

    }

    @Test
    public void testAuthenticate2() throws Exception {
        EqualsFilter filter = new EqualsFilter("uid", "gaoyiwei");
        boolean authenticate = ldapTemplate.authenticate("", filter.toString(), "111111");
        log.info("认证结果 : {}", authenticate);
    }

    @Test
    public void testSearch() throws Exception {

        LdapQuery query =  LdapQueryBuilder.query().where("uid").is("www");
        ldapTemplate.search(query,(AttributesMapper<String>) attr -> {
           /*log.info("cn:{}",String.valueOf(attr.get("cn").get()));
           log.info("uid:{}",String.valueOf(attr.get("uid").get()));
           log.info("uidNumber:{}",String.valueOf(attr.get("uidNumber").get()));*/
            NamingEnumeration<? extends Attribute> all = attr.getAll();
            log.info("ldap 可获得的用户信息：");
            while (all.hasMore()){
                Attribute next = all.next();
                log.info("{}={}",next.getID(),next.get());
            }
            return null;
        });

    }

    @Test
    public void testSearch1() throws Exception {

        LdapQuery query =  LdapQueryBuilder.query().where("uid").is("www2");
        List<String> uids = ldapTemplate.search(query, (AttributesMapper<String>) attr -> {
            String uid = attr.get("uid").get().toString();
            log.info("uid:{}", uid);
            return uid;
        });

        log.info("uids:{}",uids);

    }

    @Test
    public void testFind() throws Exception {
        LdapTemplate lt = LdapTemplateUtil.getLdapTemplate("cn=admin,dc=example,dc=com", "123456", "dc=example,dc=com", "ldap://192.168.101.22:389");
        LdapQuery query =  LdapQueryBuilder.query().where("uid").is("ww6");
        LdapUser one = lt.findOne(query, LdapUser.class);
        log.info("{}",one);

    }

    @Test
    public void testGetTemplate() throws Exception {
        LdapTemplate lt = LdapTemplateUtil.getLdapTemplate("cn=admin,dc=example,dc=com", "123456", "dc=example,dc=com", "ldap://192.168.101.22:389");
        EqualsFilter filter = new EqualsFilter("uid", "lisi");
        boolean authenticate = lt.authenticate("", filter.toString(), "123");
        log.info("认证结果 : {}", authenticate);
    }


    @Test
    public void testGetTemplate1() throws Exception {
       // LdapTemplate lt = LdapTemplateUtil.getLdapTemplate("cn=admin,dc=example,dc=com", "123456", "dc=example,dc=com", "ldap://192.168.101.22:389");
        LdapContextSource contextSource= new LdapContextSource();
        contextSource.setUrl("ldap://192.168.101.22:389");
        contextSource.setBase("dc=example,dc=com");
        contextSource.setUserDn("cn=admin,dc=example,dc=com");
        contextSource.setPassword("123456");
        LdapTemplate l = new LdapTemplate(contextSource);
        contextSource.afterPropertiesSet();

        EqualsFilter filter = new EqualsFilter("uid", "lisi");
        boolean authenticate =  l.authenticate("", filter.toString(), "123");
        log.info("认证结果 : {}", authenticate);
    }

    @Test
    public void testCheck() throws Exception {
        LdapTemplate lt = LdapTemplateUtil.getLdapTemplate("cn=admin,dc=example,dc=com6", "123456", "dc=example,dc=com", "ldap://192.168.101.22:389");
        LdapQuery query =  LdapQueryBuilder.query().where("uid").is("xxooo");
        lt.search(query,(AttributesMapper<String>) attr -> {
            return null;
        });
        log.info("连接成功");

    }



} 
