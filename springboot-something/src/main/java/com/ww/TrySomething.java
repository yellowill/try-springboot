package com.ww;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.ww.entity.Account;


@SpringBootApplication
public class TrySomething {
	
	

    public static void main(String[] args) {
        SpringApplication.run(TrySomething.class, args);
    }
    
    
    
    @Bean
    public Account newAccount() {
    	return new Account();
    }
    

}
